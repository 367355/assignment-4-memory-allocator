#include <assert.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#define BLOCK_MIN_CAPACITY 24

#define INITIAL_SIZE 4096
#define TEST_BLOCK_SIZE_1 100
#define TEST_BLOCK_SIZE_2 20
#define TEST_BLOCK_SIZE_3 17
#define TEST_BLOCK_SIZE_4 12
#define TEST_BLOCK_SIZE_5 560

static void * allocate(const struct block_header ** current, size_t query) {
    void *allocated_memory = _malloc(query);
    assert(allocated_memory != NULL);

    assert((*current)->capacity.bytes == size_max(query, BLOCK_MIN_CAPACITY));
    assert(!(*current)->is_free);
    assert((*current)->next != NULL);

    *current = (*current)->next;

    return allocated_memory;
}

static void test_successful_allocation() {
    const void *heap = heap_init(1);
    const struct block_header * head = heap;
    const struct block_header * current = head;
    assert(size_from_capacity(head->capacity).bytes == REGION_MIN_SIZE);
    assert(head->is_free);
    assert(head->next == NULL);

    allocate(&current, TEST_BLOCK_SIZE_1);

    heap_term();

    printf("test 1: passed\n");
}

static void test_free_single_block() {
    const void *heap = heap_init(1);
    const struct block_header * head = heap;
    const struct block_header * current = head;
    assert(size_from_capacity(head->capacity).bytes == REGION_MIN_SIZE);
    assert(head->is_free);
    assert(head->next == NULL);

    allocate(&current, TEST_BLOCK_SIZE_1);
    void * mem = allocate(&current, TEST_BLOCK_SIZE_2);
    allocate(&current, TEST_BLOCK_SIZE_3);

    _free(mem);
    assert(head->next->is_free);

    heap_term();
    printf("test 2: passed\n");
}

static void test_free_multiple_blocks() {
    const void *heap = heap_init(1);
    const struct block_header * head = heap;
    const struct block_header * current = head;
    assert(size_from_capacity(head->capacity).bytes == REGION_MIN_SIZE);
    assert(head->is_free);
    assert(head->next == NULL);

    allocate(&current, TEST_BLOCK_SIZE_1);
    void * mem1 = allocate(&current, TEST_BLOCK_SIZE_2);
    allocate(&current, TEST_BLOCK_SIZE_3);
    void * mem2 = allocate(&current, TEST_BLOCK_SIZE_4);
    allocate(&current, TEST_BLOCK_SIZE_5);

    _free(mem1);
    assert(head->next->is_free);
    _free(mem2);
    assert(head->next->next->next->is_free);

    heap_term();
    printf("test 3: passed\n");
}

static void test_out_of_memory_expansion() {
    const void *heap = heap_init(INITIAL_SIZE);
    const struct block_header * current = heap;
    assert(heap != NULL);

    allocate(&current, TEST_BLOCK_SIZE_1);
    allocate(&current, INITIAL_SIZE - TEST_BLOCK_SIZE_1);
    allocate(&current, TEST_BLOCK_SIZE_5);

    heap_term();
    printf("test 4: passed\n");
}

#define abs_(x) ((x) < 0 ? -(x) : (x))

static void test_out_of_memory_no_expansion() {
    const void *heap = heap_init(INITIAL_SIZE);
    const struct block_header * current = heap;
    assert(heap != NULL);

    allocate(&current, TEST_BLOCK_SIZE_1);
    void *allocated_memory2 = allocate(&current, INITIAL_SIZE - TEST_BLOCK_SIZE_1);

    void *dummy =
        mmap((void *)heap + size_max(INITIAL_SIZE, REGION_MIN_SIZE),
             INITIAL_SIZE, PROT_READ | PROT_WRITE,
             MAP_FIXED_NOREPLACE | MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);

    assert(dummy != MAP_FAILED);

    void *allocated_memory3 = _malloc(INITIAL_SIZE);

    assert(abs_(allocated_memory3 - allocated_memory2) > size_max(INITIAL_SIZE, REGION_MIN_SIZE));

    munmap(dummy, INITIAL_SIZE);
    heap_term();
    printf("test 5: passed\n");
}

int main() {
    test_successful_allocation();
    test_free_single_block();
    test_free_multiple_blocks();
    test_out_of_memory_expansion();
    test_out_of_memory_no_expansion();
    return 0;
}
